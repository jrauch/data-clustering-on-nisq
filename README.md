# Data clustering on NISQ

## Description
We uses a Mixture of QCBM (Quantum Circuit Born Machine) to clustering with a hybrid architecture CPU-QPU.

## Authors and acknowledgment

- Julien Rauch
- Damien Rontani
- Stephane Vialle

## License
 This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version. 

This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
