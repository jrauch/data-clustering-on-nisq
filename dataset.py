from sklearn.datasets import make_blobs, make_moons, make_gaussian_quantiles
from sklearn import preprocessing
import numpy as np

from sklearn.metrics import silhouette_score, davies_bouldin_score, calinski_harabasz_score
from sklearn.metrics import mutual_info_score, adjusted_rand_score, normalized_mutual_info_score


#### 2D dataset ####


def two_moon(nb_data):
    K=2
    X, labels = make_moons(n_samples=nb_data, noise=0.05)
    min_max_scaler = preprocessing.MinMaxScaler()
    X = min_max_scaler.fit_transform(X)
    return X, labels

def two_gauss_moon(nb_data):
    K=2
    min_max_scaler = preprocessing.MinMaxScaler()
    X, labels = make_blobs(n_samples = nb_data//2, centers=1)
    X2, labels2 = make_moons(n_samples=nb_data, noise=0.05)
    X2 = 8*X2[labels2==0]

    X = 0.7*min_max_scaler.fit_transform(X)  
    X[:,0] += 0.15
    X[:,1] -= 0.45
    X2 = min_max_scaler.fit_transform(X2)

    X3 = np.concatenate((X,X2),axis=0)
    X3 = min_max_scaler.fit_transform(X3)
    labels = (nb_data//2)*[0] + (nb_data//2)*[1]
    return X3, labels

def two_gauss(nb_data):
    K=2
    min_max_scaler = preprocessing.MinMaxScaler()
    div = 5
    X, labels = make_blobs(n_samples = nb_data//div, centers=1)
    X2, labels = make_blobs(n_samples = nb_data - nb_data//div, centers=1)

    X = 0.5*min_max_scaler.fit_transform(X)
    X[:,0] -=0.2
    X[:,1] -=0.2
    X2 = min_max_scaler.fit_transform(X2)

    X3 = np.concatenate((X,X2),axis=0)
    X3 = min_max_scaler.fit_transform(X3)
    labels = (nb_data//div)*[0] + (nb_data - nb_data//div)*[1]
    return X3, labels


#### 3D dataset ####

def two_moon_3D(nb_data):
    K=2
    X, labels = make_moons(n_samples=nb_data, noise=0.05)
    min_max_scaler = preprocessing.MinMaxScaler()
    X = min_max_scaler.fit_transform(X)
    new_dim_x = []
    for i in range(1000):
        aux = X[i][0]
        if labels[i] == 1:
            aux = 1 - aux
        new_dim_x.append([aux])
    new_dim_x = np.array(new_dim_x)
    X = np.concatenate((X, new_dim_x), axis=1)
    return X, labels


def two_gauss_3D(nb_data):
    K=2
    min_max_scaler = preprocessing.MinMaxScaler()
    div = 5
    X, labels = make_blobs(n_samples = nb_data//div, n_features=3, centers=1)
    X2, labels = make_blobs(n_samples = nb_data - nb_data//div, n_features=3, centers=1)

    X = 0.6*min_max_scaler.fit_transform(X)
    X[:,0] -=0.2
    X[:,1] -=0.2
    X[:,2] -=0.2
    X2 = min_max_scaler.fit_transform(X2)

    X3 = np.concatenate((X,X2),axis=0)
    X3 = min_max_scaler.fit_transform(X3)
    labels = (nb_data//div)*[0] + (nb_data - nb_data//div)*[1]
    return X3, labels


#### metric functions ####

def display_metrics(data, clustering_labels, true_labels):

    # Calculate clustering metrics
    silhouette = silhouette_score(data, clustering_labels)
    db_index = davies_bouldin_score(data, clustering_labels)
    ch_index = calinski_harabasz_score(data, clustering_labels)
    ari = adjusted_rand_score(true_labels, clustering_labels)
    mi = normalized_mutual_info_score(true_labels, clustering_labels)

    # Print the metric scores
    print(f"Silhouette Score: {silhouette:.2f}")
    print(f"Davies-Bouldin Index: {db_index:.2f}")
    print(f"Calinski-Harabasz Index: {ch_index:.2f}")
    print(f"Adjusted Rand Index: {ari:.7f}")
    print(f"Normalized Mutual Information (NMI): {mi:.7f}")


