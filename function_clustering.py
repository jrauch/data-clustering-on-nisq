from function_train import *
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import os

from joblib import dump, load
from qiskit_machine_learning.neural_networks import SamplerQNN
import qiskit_machine_learning
from qiskit_machine_learning.connectors import TorchConnector
from qiskit_algorithms.utils import algorithm_globals

def clustering(X, K, model, epoch_init, taille_batch_init, learning_rate_init, epoch_EM, taille_batch_EM, learning_rate_EM, VAL=[], PRINT=0, SAVE=0, PATH = "."):
    #### hyper parameters ###########
    #epoch1
    #taille_batch1
    #learning_rate1
    #num_point
    #epoch2
    #taille_batch2
    #learning_rate2
    #VAL

    #### make mixture of generator ###
    models = mixture_generator(K, model, X.shape[1])
    
    #### initialization of mixture ##
    kmeans = KMeans(n_clusters=K, random_state=0, n_init="auto").fit(X)
    labels = kmeans.labels_

    if PRINT:
        print("initialise models")
    init_loss = []
    for k in range(K):
        if PRINT:
            print("--",k,"--")
        loss = train1(X, models, k, labels, epoch_init, taille_batch_init, learning_rate_init, PRINT)
        init_loss.append(loss)
    
    test, l = point(models, X, labels, 500, K)
    
    ### training - EM loop ###

    if PRINT:
        print("\nEM loop")
    lij, pij, tot_loss, val_loss = train2(X.copy(), models, epoch_EM, taille_batch_EM, learning_rate_EM, VAL, PRINT)

    ### clustering label ###
    mij = mij_est(lij, pij)
    labels = label_est(mij)

    if PRINT:
        #figure_loss
        fig, axs = plt.subplots(2,2)
        for k in range(K):
            axs[0,0].plot(init_loss[k], label="model "+str(k))
        axs[0,0].set_title("loss initialisation")
        axs[0,1].plot(tot_loss, label="data")
        if len(VAL) != 0:
            axs[0,1].plot(val_loss, label="validation")
        axs[0,1].set_title("loss mixture")
        axs[1,0].scatter(test[:, 0], test[:, 1],c=l)
        axs[1,0].set_title("output after initial")
        test, l = point(models, X, labels, 500, K)
        axs[1,1].scatter(test[:, 0], test[:, 1],c=l)
        axs[1,1].set_title("output in the end")
        #fig.legend()
    
    if SAVE:
        if not os.path.exists(PATH):
            os.makedirs(PATH)
        models.save(PATH)

    return labels