import torch
import numpy as np
from torch.utils.data import TensorDataset
import torch.nn.functional as F
import math
import torch.nn as nn
import os 
import time

from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF

################ Mixture of Generative Models ################

class mixture_generator:
    def __init__(self, num, Generator, dim):
        """ 
        num : number of cluster/generator
        Generator : python class of a Generator model (see folder generator)
        dim : dim of data
        """
        self.generator_ = []
        self.num = num
        for i in range(num):
            self.generator_.append(Generator(dim))
    
    def sample(self, num, nb):
        """
        num : what cluster/generator
        nb : nomber of data to generer
        """
        return self.generator_[num].sample(nb)
    
    def parameter(self, num):
        """return number paramters of num generator"""
        self.generator_[num].parameters()
    
    def save(self, path):
        """ save model in path
            *work only with pytorch model 
        """
        i=0
        for model in self.generator_:
            model_path =  path+"/model"+str(i)+".pt"
            model.save(model_path)
            #torch.save(model.state_dict(), path+"/model"+str(i)+".pt")
            i+=1
    
    def load(self, path):
        """ load model of path
            *work only with pytorch model
        """
        for i in range(self.num):
            model_path =  path+"/model"+str(i)+".pt"
            self.generator_[i].load(model_path)


################ classifiers for E step ################

from sklearn.neighbors import KernelDensity


class classifier():
    def __init__(self, n_cluster, kernel='gaussian', bandwidth=0.05):
        self.n_cluster = n_cluster
        self.kernel = kernel
        self.bandwidth = bandwidth #"silverman" #0.1
        self.kde = []
        for i in range(n_cluster):
            self.kde.append(KernelDensity(kernel=kernel, bandwidth=self.bandwidth))
    
    def fit(self, X, i):
        self.kde[i].fit(X)
    
    def fit(self, X):
        self.kde = []
        for i in range(self.n_cluster):
            self.kde.append(KernelDensity(kernel=self.kernel, bandwidth=self.bandwidth))
        for i in range(self.n_cluster):
            self.kde[i].fit(X[i])

    def score(self, X):
        res = []
        for i in range(self.n_cluster):
            log_density = self.kde[i].score_samples(X)
            res.append(np.exp(log_density))
        res = np.array(res)
        res = np.transpose(res)
        return res
    
    def sample(self, nb):
        res = []
        #sample
        for i in range(self.n_cluster):
            res.append(self.kde[i].sample(nb))
        return res





################ Function for initialization ################


def train1(X, models, num, label, epochs, batch_size, learning_rate, PRINT=0):
    data = X[np.nonzero(label == num)]
    dataset = torch.from_numpy(data)
    Dataset = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle = True)

    total_loss = models.generator_[num].train(Dataset, epochs, learning_rate)

    return total_loss


################ Function for training ################


def lij_est(X, models, classifier, L, num_point):
    """return the probability of each data Xi to be in each ckuster j"""
    K = models.num
    samples = []
    for i in range(K):
        new_sample = models.sample(i, num_point).detach().numpy()
        samples.append(new_sample)
    classifier.fit(samples)
    return classifier.score(X)
    


def mij_est(lij, pi_j):
    """"return the probability of data X[i] to be in cluster j weighted by pi[j]"""
    res = []
    N = lij.shape[1]
    M = lij.shape[0]
    for i in range(M):
        aux = []
        for j in range(N):
            aux.append(pi_j[j] * lij[i,j])
        aux = np.array(aux)
        res.append(aux/np.sum(aux)) # normalize
    return np.array(res)

def pij_est(mij):
    """update pi the importance of cluster, pi[j] for cluster j"""
    N=mij.shape[0]
    K=mij.shape[1]
    res = []
    for k in range(K):
        res.append(mij[:,k].sum()/N)
    return res


def split_dataset(X, mij):
    """split data in different dataset according the their probability to bellows to cluster"""
    limit = 0.4
    N=mij.shape[0]
    K=mij.shape[1]
    res = []
    for j in range(K):
        dataset = []
        for i in range(N):
            if mij[i][j] > limit:
                dataset.append(X[i])
        res.append(dataset)
    return res



def train2(X, models, epochs, batch_size, learning_rate, VAL, PRINT=0):
    """Exeptation Maximization algorithm"""
    evol = epochs//5
    clf = classifier(models.num)

    L = 0.05
    num_point = 400
    pi_j = models.num*[1/models.num]

    total_loss = []
    val_loss = [] #TODO

    for e in range(epochs):
        # Exeptation
        lij = lij_est(X, models, clf, L, num_point)
        mij = mij_est(lij, pi_j)
        pi_j = pij_est(mij)
        
        dataset = split_dataset(X, mij)
        # Maximization
        data_loss = 0
        for i in range(models.num):
            Dataset = torch.utils.data.DataLoader(dataset[i], batch_size=batch_size, shuffle = False)
            loss = models.generator_[i].train(Dataset, 1, learning_rate)
            data_loss += loss[0]
        total_loss.append(data_loss)
        if e%evol==0 and PRINT:
            print("epoch : ", e,"/", epochs, "  -- loss :", data_loss)

    
    lij = lij_est(X, models, clf, L, num_point)
    mij = mij_est(lij, pi_j)
    pi_j = pij_est(mij)

    return lij, pi_j, total_loss, val_loss

#### tools functions ####

def point(models, X, labels, nb_data, K):
    m_test = []
    res = X
    l = labels
    for k in range(K):
        m_test = models.sample(k, nb_data//K).detach().numpy()
        l = np.concatenate((l,(nb_data//K)*[K+k]))
        res = np.concatenate((res, m_test))
    return res, l


def label_est(mij):
    N = mij.shape[1]
    M = mij.shape[0]
    labels = []
    for i in range(M):
        aux = 0
        label = 0
        for j in range(N):
            if mij[i][j]>aux:
                aux = mij[i][j]
                label = j
        labels.append(label)
    return labels