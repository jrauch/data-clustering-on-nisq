import torch
import torch.nn as nn
from qiskit import Aer, execute
from qiskit import QuantumCircuit
import numpy as np
import math
from qiskit.circuit import Parameter, ParameterVector
from qiskit_machine_learning.neural_networks import SamplerQNN, EstimatorQNN
from qiskit_machine_learning.connectors import TorchConnector
from qiskit_algorithms.utils import algorithm_globals



### Model for classical Network ###

class Generator_classique(nn.Module):
    def __init__(self, input_dim, output_dim):
        super(Generator_classique, self).__init__()
        self.model = nn.Sequential(
            nn.Linear(input_dim, 32),
            nn.ReLU(),
            nn.Linear(32, 128),
            nn.ReLU(),
            nn.Linear(128, 32),
            nn.ReLU(),
            nn.Linear(32, output_dim),
            #nn.Tanh()  # Using Tanh activation for the final output to ensure values are in the range [-1, 1]
            nn.Sigmoid()  # Using Sigmoid activation to get a probability value between 0 and 1
        )

    def forward(self, x):
        return self.model(x)





### Model for continuous QCBM ###


def circuit(dim, reap, sampler_test):
    qc = QuantumCircuit(dim)
    noise = ParameterVector('in', dim)
    param = ParameterVector('p', reap*3*dim)

    for r in range(reap):
        for i in range(dim):
            qc.rx(noise[i],i)
        for i in range(dim):
            for j in range(dim):
                if i!=j:
                    qc.cx(i,j)
        for i in range(dim):
            qc.rz(param[6*r + 3*i],i)
            qc.ry(param[6*r + 3*i+1],i)
            qc.rz(param[6*r + 3*i+2],i)

    qnn = SamplerQNN(
    circuit=qc,
    sampler=sampler_test,
    input_params=noise,
    weight_params=param
    )
    return qnn


class QCBM_continue(nn.Module):
    def __init__(self, dim_in, dim_out, sampler=None):
        super(QCBM_continue, self).__init__()
        self.dim = dim_in
        self.sampler = sampler

        #n_q = int(math.log(div**dim, 2))
        qnn = circuit(self.dim, 2, self.sampler)
        initial_weights = 0.1 * (2 * algorithm_globals.random.random(qnn.num_weights) - 1)
        self.qc = TorchConnector(qnn, initial_weights=initial_weights)

    def forward(self,x):
        out = self.qc(x)
        res  = 0.5*(1 + out[:, 0] + out[:, 1] - out[:, 2] - out[:, 3])
        res = torch.stack((res, 0.5*(1 + out[:, 0] - out[:, 1] + out[:, 2] - out[:, 3])), 1)
        return res



### Model for noisy continuous QCBM ###

def noisy_QCBM(sampler_noisy):
    class QCBM_continue(nn.Module):
        def __init__(self, dim_in, dim_out, sampler=sampler_noisy):
            super(QCBM_continue, self).__init__()
            self.dim = dim_in
            self.sampler = sampler

            #n_q = int(math.log(div**dim, 2))
            qnn = circuit(self.dim, 2, self.sampler)
            initial_weights = 0.1 * (2 * algorithm_globals.random.random(qnn.num_weights) - 1)
            self.qc = TorchConnector(qnn, initial_weights=initial_weights)

        def forward(self,x):
            out = self.qc(x)
            res  = 0.5*(1 + out[:, 0] + out[:, 1] - out[:, 2] - out[:, 3])
            res = torch.stack((res, 0.5*(1 + out[:, 0] - out[:, 1] + out[:, 2] - out[:, 3])), 1)
            return res
    return QCBM_continue




### Model for discrete QCBM ###

def circuit_discret(dim, div, sampler_test):
    nb_qubits = int(div//dim)
    qc = QuantumCircuit(nb_qubits)
    param = ParameterVector('p', 3*nb_qubits-2)
    for i in range(nb_qubits):
        qc.ry(param[i],i)
    for i in range(nb_qubits-1):
        qc.ryy(param[nb_qubits+i],i,i+1)
    for i in range(nb_qubits-1):
        qc.cry(param[2*nb_qubits-1+i],i,i+1)
    #qc.measure_all()
    qnn = SamplerQNN(
    circuit=qc,
    sampler=sampler_test,
    weight_params=param)
    return qnn


class QCBM_discret(nn.Module):
    def __init__(self, dim, div, sampler=None):
        super(QCBM_discret, self).__init__()
        self.dim = dim
        self.div = div
        self.sampler = sampler

        #n_q = int(math.log(div**dim, 2))
        qnn = circuit_discret(self.dim, self.div, self.sampler)
        initial_weights = 0.1 * (2 * algorithm_globals.random.random(qnn.num_weights) - 1)
        self.qc = TorchConnector(qnn, initial_weights=initial_weights)

    def forward(self):
        res = self.qc()
        return res



### Model for noisy discrete QCBM ###

def noisy_QCBM_discret(sampler_noisy):
    class QCBM_discret(nn.Module):
        def __init__(self, dim, div, sampler=sampler_noisy):
            super(QCBM_discret, self).__init__()
            self.dim = dim
            self.div = div
            self.sampler = sampler

            #n_q = int(math.log(div**dim, 2))
            qnn = circuit_discret(self.dim, self.div, self.sampler)
            initial_weights = 0.1 * (2 * algorithm_globals.random.random(qnn.num_weights) - 1)
            self.qc = TorchConnector(qnn, initial_weights=initial_weights)

        def forward(self):
            res = self.qc()
            return res
    return QCBM_discret