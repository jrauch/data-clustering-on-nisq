import torch
from torch import nn, optim, autograd
from qiskit import QuantumCircuit
import numpy as np
import math
from qiskit.circuit import Parameter, ParameterVector
from qiskit_machine_learning.neural_networks import SamplerQNN, EstimatorQNN
from qiskit_machine_learning.connectors import TorchConnector
from qiskit_algorithms.utils import algorithm_globals
from qiskit.transpiler.preset_passmanagers import generate_preset_pass_manager

algorithm_globals.random_seed = 42

def circuit(dim, reap, sampler):
    qc = QuantumCircuit(dim)
    noise = ParameterVector('in', dim)
    param = ParameterVector('p', reap*3*dim)

    for r in range(reap):
        for i in range(dim):
            qc.rx(noise[i],i)
        for i in range(dim):
            for j in range(dim):
                if i!=j and (i==j+1 or j==i+1):
                    qc.cx(i,j)
        for i in range(dim):
            qc.rz(param[3*dim*r + 3*i],i)
            qc.ry(param[3*dim*r + 3*i+1],i)
            qc.rz(param[3*dim*r + 3*i+2],i)
        qc.barrier()
    qc.measure_all()

    pm = None
    if sampler != None:
        pm = generate_preset_pass_manager(backend=sampler.backend, optimization_level=1)
    
    qnn = SamplerQNN(
    circuit=qc,
    sampler=sampler,
    input_params=noise,
    weight_params=param,
    #gradient=None,
    input_gradients=True,
    pass_manager=pm
    )
    return qnn


def Z_obs(prob):
    nb = int(math.log2(prob.shape[1]))
    I = torch.eye(2)
    Z = torch.tensor([[1,0],[0,-1]])
    observableMat=[]
    for i in range(nb):
        aux = I
        if i==0:
            aux=Z
        for j in range(nb-1): 
            if (j+1)==i:
                aux = torch.kron(aux,Z)
            else:
                aux = torch.kron(aux,I)
        observableMat.append(aux)
    observableMat = torch.stack(observableMat,0)

    return torch.stack([torch.stack([(torch.sum(torch.matmul(obs, proba))+1)/2 for obs in observableMat],0) for proba in prob],0)

# https://www.kaggle.com/code/onurtunali/maximum-mean-discrepancy
def loss_MMD(x,y):
    L = 0.1
    xx, yy, zz = torch.mm(x, x.t()), torch.mm(y, y.t()), torch.mm(x, y.t())
    rx = (xx.diag().unsqueeze(0).expand_as(xx))
    ry = (yy.diag().unsqueeze(0).expand_as(yy))
    dxx = rx.t() + rx - 2. * xx 
    dyy = ry.t() + ry - 2. * yy 
    dxy = rx.t() + ry - 2. * zz
    XX = torch.exp(-1*dxx/(2*L*L))
    YY = torch.exp(-1*dyy/(2*L*L))
    XY = torch.exp(-1*dxy/(2*L*L))

    return torch.mean(XX + YY - 2. * XY)

class QCBM_c(nn.Module):
    def __init__(self, dim, sampler=None):
        super(QCBM_c, self).__init__()
        self.dim = dim
        self.sampler = sampler

        qnn = circuit(self.dim, 2, self.sampler)
        initial_weights = 0.1 * (2 * algorithm_globals.random.random(qnn.num_weights) - 1)
        #initial_weights = algorithm_globals.random.random(qnn.num_weights)
        self.qc = TorchConnector(qnn, initial_weights=initial_weights)

    def forward(self,x):
        out = self.qc(x)
        res = Z_obs(out)
        return res



def Continuous_QCBM(sampler=None):
    class Continuous_QCBM():
        def __init__(self, dim, device='cpu'):
            self.device = device
            self.dim = dim
            self.QCBM = QCBM_c(dim, sampler)
    
        def train(self, Dataset, epochs, learning_rate):
            #train(self.Network , Dataset, batch_size, epochs, learning_rate, learning_rate, self.device, PRINT=0)
            optimizer = optim.Adam(self.QCBM.parameters(), lr=learning_rate)
            #optimizer = torch.optim.SGD(self.QCBM.parameters(), lr=learning_rate, momentum=0.9)

            total_loss = []
            for epoch in range(epochs):        # 1. train D first
                loss_data = 0
                for xr in Dataset:
                    optimizer.zero_grad(set_to_none=True)  # Initialize gradient
                    z = math.pi*(2*torch.rand(xr.shape[0], self.dim).to(self.device)-1)/2
                    sample = self.QCBM(z)
                    sample = sample.double()
                    loss = loss_MMD(xr, sample)
                    loss.backward()  # Backward pass
                    optimizer.step()  # Optimize weights
                    loss_data += loss.detach().item()
                total_loss.append(loss_data)
            return total_loss
    
        def sample(self, nb):
            z = math.pi*(2*torch.rand(nb, self.dim).to(self.device)-1)/2
            return self.QCBM(z)
    
        def parameters(self):
            params = sum(p.numel() for p in self.QCBM.parameters() if p.requires_grad)
            print("parameters Continuous QCBM :", params)
            
        def save(self, path):
            torch.save(self.QCBM.state_dict(), path)
    
        def load(self, path):
            self.QCBM.load_state_dict(torch.load(path))
    return Continuous_QCBM