import torch
from torch import nn, optim, autograd
from qiskit import QuantumCircuit
import numpy as np
import math
from qiskit.circuit import Parameter, ParameterVector
from qiskit_machine_learning.neural_networks import SamplerQNN, EstimatorQNN
from qiskit_machine_learning.connectors import TorchConnector
from qiskit_algorithms.utils import algorithm_globals
from qiskit.transpiler.preset_passmanagers import generate_preset_pass_manager


def circuit_discret(dim, div, sampler_test):
    nb_qubits = int(dim*math.log2(div))
    qc = QuantumCircuit(nb_qubits)
    param = ParameterVector('p', 3*nb_qubits-2)
    for i in range(nb_qubits):
        qc.ry(param[i],i)
    for i in range(nb_qubits-1):
        qc.ryy(param[nb_qubits+i],i,i+1)
    for i in range(nb_qubits-1):
        qc.cry(param[2*nb_qubits-1+i],i,i+1)
    qc.measure_all()
    pm = None
    if sampler_test != None:
        pm = generate_preset_pass_manager(backend=sampler_test.backend, optimization_level=1)

    qnn = SamplerQNN(
    circuit=qc,
    sampler=sampler_test,
    weight_params=param,
    #gradient=None,
    input_gradients=True,
    pass_manager=pm
    )
    return qnn


class QCBM_d(nn.Module):
    def __init__(self, dim, div, sampler=None):
        super(QCBM_discret, self).__init__()
        self.dim = dim
        self.div = div
        self.sampler = sampler

        #n_q = int(math.log(div**dim, 2))
        qnn = circuit_discret(self.dim, self.div, self.sampler)
        initial_weights = 0.1 * (2 * algorithm_globals.random.random(qnn.num_weights) - 1)
        self.qc = TorchConnector(qnn, initial_weights=initial_weights)

    def forward(self):
        res = self.qc()
        return res



class QCBM_d(nn.Module):
    def __init__(self, dim, div, sampler=None):
        super(QCBM_d, self).__init__()
        self.dim = dim
        self.div = div
        self.sampler = sampler

        qnn = circuit_discret(self.dim, self.div, self.sampler)
        initial_weights = 0.1 * (2 * algorithm_globals.random.random(qnn.num_weights) - 1)
        self.qc = TorchConnector(qnn, initial_weights=initial_weights)

    def forward(self):
        res = self.qc()
        return res


def ckl(Q,P):
        eps = 1e-12
        Q = Q+eps
        P = P+eps
        Q = Q/Q.sum()
        P = P/P.sum()
        res = Q * torch.log(Q/P)
        return torch.sum(res)

def histogram_data_func(Dataset, dim, div, range_):
    #point = Dataset
    #histog, edges = np.histogramdd(point, bins = div, range=RANGE)
    #histog = histog.reshape(div**dim)
    #histog = histog / np.sum(histog)
    #P = torch.tensor(histog)

    #input = next(iter(Dataset))
    input = None
    for xr in Dataset:
        if input == None:
            input = xr
        else:
            input = torch.cat((input,xr))
    if input == None:
        print("error empty")
        P = (div**dim)*[1/(div**dim)]
        P = torch.tensor(P)
    else:
        P, bin_edges = torch.histogramdd(input, bins=div, range=range_, density=True)
        P = P.reshape(div**dim)
    return P/P.sum()

def param(net):
    res = []
    for name, param in net.named_parameters():
        if param.requires_grad:
            res.append(param.data)
    return res

import torch.nn.functional as F

def Discrete_QCBM(div, range_, sampler=None):
    class Discrete_QCBM():
        def __init__(self, dim, device='cpu'):
            self.device = device
            self.dim = dim
            self.QCBM = QCBM_d(dim, div, sampler)
            self.div = div
            self.range = range_
    
        def train(self, Dataset, epochs, learning_rate):
            histogram_data = histogram_data_func(Dataset, self.dim, self.div, self.range)

            optimizer = optim.Adam(self.QCBM.parameters(), lr=learning_rate)
            #loss_KL = nn.KLDivLoss(reduction="batchmean")
            total_loss = []
            for epoch in range(epochs):        # 1. train D first
                optimizer.zero_grad(set_to_none=True)  # Initialize gradient
                #print(param(self.QCBM))
                histogram = self.QCBM()
                    
                loss = ckl(histogram, histogram_data)
                #loss = loss_KL(histogram, histogram_data)
                loss.backward()  # Backward pass
                optimizer.step()  # Optimize weights
                total_loss.append(loss.detach().item())
            return total_loss
    
        def sample(self, nb):
            histogram = self.QCBM()
            #histogram = histogram.reshape(self.dim*(self.div,))
            div = self.div
            point = histogram.multinomial(num_samples=nb, replacement=True)
            def indice(position):
                res = []
                aux = position
                for i in range(self.dim):
                    res.append(aux // (div**(self.dim-1-i)))
                    aux = aux % (div**(self.dim-1-i))
                return torch.tensor(res)
            m_points = [torch.rand(self.dim) + indice(pos) for pos in point]
            return (1/div)*torch.stack(m_points)
    
        def parameters(self):
            params = sum(p.numel() for p in self.QCBM.parameters() if p.requires_grad)
            print("parameters Discrete QCBM :", params)
        
        def save(self, path):
            torch.save(self.QCBM.state_dict(), path)
    
        def load(self, path):
            self.QCBM.load_state_dict(torch.load(path))
    return Discrete_QCBM



