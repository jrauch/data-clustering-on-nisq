import torch
from torch import nn, optim, autograd
import numpy as np

class Generator_classique(nn.Module):
    def __init__(self, dim):
        super(Generator_classique, self).__init__()
        self.model = nn.Sequential(
            nn.Linear(dim, 32),
            nn.ReLU(),
            nn.Linear(32, 128),
            nn.ReLU(),
            nn.Linear(128, 32),
            nn.ReLU(),
            nn.Linear(32, dim),
            nn.Sigmoid()
        )

    def forward(self, x):
        return self.model(x)


# https://www.kaggle.com/code/onurtunali/maximum-mean-discrepancy
def loss_MMD(x,y, L):
    L = 0.05
    xx, yy, zz = torch.mm(x, x.t()), torch.mm(y, y.t()), torch.mm(x, y.t())
    rx = (xx.diag().unsqueeze(0).expand_as(xx))
    ry = (yy.diag().unsqueeze(0).expand_as(yy))
    dxx = rx.t() + rx - 2. * xx 
    dyy = ry.t() + ry - 2. * yy 
    dxy = rx.t() + ry - 2. * zz
    XX = torch.exp(-1*dxx/(2*L*L))
    YY = torch.exp(-1*dyy/(2*L*L))
    XY = torch.exp(-1*dxy/(2*L*L))

    return torch.mean(XX + YY - 2. * XY)



class NN():
    def __init__(self, dim, device='cpu'):
        self.device = device
        self.dim = dim
        self.Network = Generator_classique(dim)
    
    def train(self, Dataset, epochs, learning_rate):
        #train(self.Network , Dataset, batch_size, epochs, learning_rate, learning_rate, self.device, PRINT=0)
        optimizer = optim.Adam(self.Network.parameters(), lr=learning_rate)
        #optimizer = torch.optim.SGD(self.Network.parameters(), lr=learning_rate, momentum=0.9)
        total_loss = []
        for epoch in range(epochs):        # 1. train D first
            loss_data = 0
            for xr in Dataset:
                optimizer.zero_grad(set_to_none=True)  # Initialize gradient
                z = torch.randn(xr.shape[0], self.dim).to(self.device)
                sample = self.Network(z)
                sample = sample.double()
                loss = loss_MMD(xr, sample, 0.05)
                loss.backward()  # Backward pass
                optimizer.step()  # Optimize weights
                loss_data += loss.detach().item()
            total_loss.append(loss_data)
        return total_loss
    
    def sample(self, nb):
        z = torch.randn(nb, self.dim).to(self.device)
        return self.Network(z)
    
    def parameters(self):
        params = sum(p.numel() for p in self.Network.parameters() if p.requires_grad)
        print("parameters Neural Network :", params)
    
    def save(self, path):
        torch.save(self.Network.state_dict(), path)
    
    def load(self, path):
        self.Network.load_state_dict(torch.load(path))